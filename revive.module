<?php

/**
 * @file
 * Module file for revive module.
 */

/**
 * @defgroup revive Revive Ad Server Integration
 * @{
 * Revive AdServer Integration with nodes and taxonomy terms
 */

/**
 * Implements hook_permission
 */
function revive_permission() {
  return array(
    'administer revive' => array(
      'title' => t('Administer revive Ad Server'),
      'description' => t('Allow users to administer revive Ad Server settings'),
    ),
  );
}

/**
 * Implements hook_menu().
 *
 * Register revive settings forms.
 */
function revive_menu() {
  $items['admin/config/services/revive'] = array(
    'title' => 'revive Adserver',
    'description' => 'Configure the revive Adserver for serving ads on the website',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('revive_settings'),
    'access arguments' => array('administer revive'),
  );
  $items['admin/config/services/revive/general-settings'] = array(
    'title' => t('General'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
    'access arguments' => array('administer revive'),
  );

  $items['admin/config/services/revive/entity-settings'] = array(
    'title' => t('Choose entities'),
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('revive_entity_settings'),
    'access arguments' => array('administer revive'),
  );

  return $items;
}

function revive_entity_settings($form, &$form_state) {
  $node_bundles = field_info_bundles('node');
  $node_bundles_options = array();

  foreach ($node_bundles as $bundle_name => $bundle_data) {
    $node_bundles_options[$bundle_name] = $bundle_data['label'];
  }

  $form['revive_node_bundles'] = array(
    '#title' => t('Node'),
    '#type' => 'checkboxes',
    '#options' => $node_bundles_options,
    '#default_value' => variable_get('revive_node_bundles', array()),
  );

  $taxonomy_term_bundles = field_info_bundles('taxonomy_term');
  $taxonomy_term_bundles_options = array();

  foreach ($taxonomy_term_bundles as $bundle_name => $bundle_data) {
    $taxonomy_term_bundles_options[$bundle_name] = $bundle_data['label'];
  }


  $form['revive_taxonomy_term_bundles'] = array(
    '#title' => t('Taxonomy'),
    '#type' => 'checkboxes',
    '#options' => $taxonomy_term_bundles_options,
    '#default_value' => variable_get('revive_taxonomy_term_bundles', array()),
  );

  $form['#submit'][] = 'revive_entity_settings_submit';
  return system_settings_form($form);
}

function revive_entity_settings_submit($form, &$form_state) {
  $field_name = 'field_revive_ad';
  foreach ($form_state['values']['revive_node_bundles'] as $node_bundle => $value) {
    $field_instance = field_info_instance('node', $field_name, $node_bundle);
    if ($value !== 0) {
      if (!$field_instance) {
        $instance = array(
          'field_name' => $field_name,
          'entity_type' => 'node',
          'bundle' => $node_bundle,
          'label' => t('revive inline video ads'),
          'description' => t('Choose the inline video ad to display on the node'),
        );
        field_create_instance($instance);
      }
    } else {
      if (!empty($field_instance)) {
        field_delete_instance($field_instance, FALSE);
      }
    }
  }
  foreach ($form_state['values']['revive_taxonomy_term_bundles'] as $taxonomy_term_bundle => $value) {
    $field_instance = field_info_instance('taxonomy_term', $field_name, $taxonomy_term_bundle);
    if ($value !== 0) {
      if (!$field_instance) {
        $instance = array(
          'field_name' => $field_name,
          'entity_type' => 'taxonomy_term',
          'bundle' => $taxonomy_term_bundle,
          'label' => t('revive inline video ads'),
          'description' => t('Choose the inline video ad to display on the taxonomy term'),
        );
        field_create_instance($instance);
      }
    } else {
      if (!empty($field_instance)) {
        field_delete_instance($field_instance, FALSE);
      }
    }
  }
}

function revive_settings($form, &$form_state) {
  $form['admin_details'] = array(
    '#title' => t('revive Adserver administrator credentials'),
    '#type' => 'fieldset',
    '#description' => t('Administration account to access revive Adserver XMLRPC API'),
  );

  $form['admin_details']['revive_server_url'] = array(
    '#title' => t('Server URL'),
    '#type' => 'textfield',
    '#default_value' => variable_get('revive_server_url', ''),
    '#max_length' => 64,
    '#size' => 32,
    '#required' => TRUE,
  );

  $form['admin_details']['revive_user'] = array(
    '#title' => t('Username'),
    '#type' => 'textfield',
    '#default_value' => variable_get('revive_user', ''),
    '#max_length' => 64,
    '#size' => 15,
    '#required' => TRUE,
  );
  $form['admin_details']['revive_pass'] = array(
    '#title' => t('Password'),
    '#type' => 'textfield',
    '#default_value' => variable_get('revive_pass', ''),
    '#max_length' => 64,
    '#size' => 15,
    '#required' => TRUE,
  );

  $form['admin_details']['set_credentials'] = array(
    '#title' => t('Set credentials'),
    '#type' => 'button',
    '#value' => t('Set credentials'),
    '#name' => 'set_credentials',
    '#element_validate' => array(
      'revive_validate_credentials',
    ),
    '#states' => array(
      'visible' => array(
        ':input[name="revive_server_url"]' => array(
          'filled' => TRUE,
        ),
        ':input[name="revive_user"]' => array(
          'filled' => TRUE,
        ),
        ':input[name="revive_pass"]' => array(
          'filled' => TRUE,
        ),
      ),
    ),
    '#ajax' => array(
      'callback' => 'revive_validate_callback',
      'wrapper' => 'general-div',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  $form['general_settings'] = array(
    '#title' => t('revive Adserver general settings'),
    '#type' => 'fieldset',
    '#description' => t('General settings for choosing agency, publisher and zone for advertising'),
    '#prefix' => '<div id="general-div">',
    '#suffix' => '</div>',
  );

  // Get agency list from revive Adserver
  if (isset($form_state['triggering_element'])) {
      $revive_server_url = $form_state['values']['revive_server_url'];
      $revive_user = $form_state['values']['revive_user'];
      $revive_pass = $form_state['values']['revive_pass'];
  } else {
    $revive_server_url = variable_get('revive_server_url', '');
    $revive_user = variable_get('revive_user', '');
    $revive_pass = variable_get('revive_pass', '');
  }
  $agency_options = array();
  if (!empty($revive_server_url) && !empty($revive_user) && !empty($revive_pass)) {
    $server = url($revive_server_url.'/www/api/v2/xmlrpc/', array(
      'external' => TRUE,
    ));
    $options = array(
      'ox.logon' => array(
        $revive_user,
        $revive_pass,
      ),
    );
    $session_id = xmlrpc($server, $options);
    if ($session_id === FALSE) {
      drupal_set_message(t('Error return from xmlrpc(): Error: @errno, Message: @message',
      array('@errno' => xmlrpc_errno(), '@message' => xmlrpc_error_msg())),
      'error');
    } else {
      $options = array(
        'ox.getAgencyList' => array(
          $session_id,
        ),
      );
      $agency_list = xmlrpc($server, $options);
      if ($agency_list === FALSE) {
        drupal_set_message(t('Error return from xmlrpc(): Error: @errno, Message: @message',
        array('@errno' => xmlrpc_errno(), '@message' => xmlrpc_error_msg())),
        'error');
      } else {
        foreach ($agency_list as $agency) {
          $agency_options[$agency['agencyId']] = $agency['agencyName'];
        }
      }
    }
  }

  $form['general_settings']['revive_agency'] = array(
    '#title' => t('Agency'),
    '#type' => 'select',
    '#options' => $agency_options,
    '#default_value' => variable_get('revive_agency', ''),
    '#ajax' => array(
      'callback' => 'revive_agency_callback',
      'wrapper' => 'agency-dependent-div',
    ),
    '#empty_option' => t('- Select -'),
    // '#required' => TRUE,
  );

  $form['general_settings']['agency_dependent_settings'] = array(
    '#type' => 'fieldset',
    '#prefix' => '<div id="agency-dependent-div">',
    '#suffix' => '</div>',
  );

  $website_options = array();
  $agency = !empty($agency_options) ? isset($form_state['values']['revive_agency']) ? $form_state['values']['revive_agency'] : variable_get('revive_agency', '') : '';
  if (!empty($agency)) {
    $websites = xmlrpc($server, array(
      'ox.getPublisherListByAgencyId' => array(
        $session_id,
        (int)$agency,
      ),
    ));
    if ($websites === FALSE) {
      drupal_set_message(t('Error return from xmlrpc(): Error: @errno, Message:  @message',
      array('@errno' => xmlrpc_errno(), '@message' => xmlrpc_error_msg())), 'error');
    } else {
      foreach ($websites as $website) {
        $website_options[$website['publisherId']] = $website['publisherName'];
      }
    }
  }

  $form['general_settings']['agency_dependent_settings']['revive_website'] = array(
    '#title' => t('Website'),
    '#type' => 'select',
    '#options' => $website_options,
    '#default_value' => isset($form_state['values']['revive_website']) ? $form_state['values']['revive_website'] : variable_get('revive_website', ''),
    '#ajax' => array(
      'callback' => 'revive_website_callback',
      'wrapper' => 'zone-div',
    ),
    '#empty_option' => t('- None -'),
  );


  $zone_options = array();
  $website = !empty($website_options) ? isset($form_state['values']['revive_website']) ? $form_state['values']['revive_website'] : variable_get('revive_website', '') : '';
  if (!empty($website)) {
    $zones = xmlrpc($server, array(
      'ox.getZoneListByPublisherId' => array(
        $session_id,
        (int)$website,
      ),
    ));
    if ($zones === FALSE) {
      drupal_set_message(t('Error return from xmlrpc(): Error: @errno, Message:  @message',
      array('@errno' => xmlrpc_errno(), '@message' => xmlrpc_error_msg())), 'error');
    } else {
      foreach ($zones as $zone) {
        // Add the zone only if zone type is 6, i.e. Inline Video ad
        if ($zone['type'] == 6) {
          $zone_options[$zone['zoneId']] = $zone['zoneName'];
        }
      }
    }
  }
  $form['general_settings']['agency_dependent_settings']['revive_zone'] = array(
    '#title' => t('Zone'),
    '#type' => 'select',
    '#options' => $zone_options,
    '#default_value' => isset($form_state['values']['revive_zone']) ? $form_state['values']['revive_zone'] : variable_get('revive_zone', ''),
    '#prefix' => '<div id="zone-div">',
    '#suffix' => '</div>',
    '#empty_option' => t('- None -'),
    '#states' => array(
      'visible' => array(
        ':input[name="revive_website"]' => array(
          'filled' => TRUE,
        ),
      ),
      'required' => array(
        ':input[name="revive_website"]' => array(
        'filled' => TRUE,
        ),
      ),
    ),
  );

  return system_settings_form($form);
}

function revive_validate_credentials($element, &$form_state, $form) {

  $revive_server_url = $form_state['values']['revive_server_url'];
  $revive_user = $form_state['values']['revive_user'];
  $revive_pass = $form_state['values']['revive_pass'];

  $options = array(
    'ox.logon' => array(
      $revive_user,
      $revive_pass,
    ),
  );

  $session_id = xmlrpc($revive_server_url.'/www/api/v2/xmlrpc/', $options);

  if ($session_id === FALSE) {
    drupal_set_message(t('Error return from xmlrpc(): Error: @errno, Message:  @message',
    array('@errno' => xmlrpc_errno(), '@message' => xmlrpc_error_msg())), 'error');
  }
}

function revive_validate_callback($form, $form_state) {
  return $form['general_settings'];
}

function revive_agency_callback($form, $form_state) {
  return $form['general_settings']['agency_dependent_settings'];
}

function revive_website_callback($form, $form_state) {
  return $form['general_settings']['agency_dependent_settings']['revive_zone'];
}

/**
 * Implements hook_node_view()
 */
function revive_node_view($node, $view_mode, $langcode) {
    // if videojs-vast-vpaid module is enabled, Revive will load
    // ads on the Video.js player with it
    $revive_node_bundles = variable_get('revive_node_bundles', array());
    $revive_taxonomy_term_bundles = variable_get('revive_taxonomy_term_bundles', array());

    if (!empty($revive_node_bundles) || !empty($revive_taxonomy_term_bundles)) {
      $revive_server_url = variable_get('revive_server_url', '');
      $revive_zone = variable_get('revive_zone', '');

      $invocation_string = $revive_server_url.'/www/delivery/fc.php?script=bannerTypeHtml:vastInlineBannerTypeHtml:vastInlineHtml&format=vast&nz=1&zones=pre-roll%3D'.$revive_zone;

      if (!empty($revive_node_bundles) && in_array($node->type, $revive_node_bundles)) {
        $invocation_string .= '&nid='.$node->nid;
      }

      if (!empty($revive_taxonomy_term_bundles)) {
        $tids = array();
        foreach (array_keys(array_filter(field_info_field_map(), function($element) {
          return $element['type'] == 'taxonomy_term_reference';
        })) as $field) {
          $items = field_get_items('node', $node, $field, $langcode);
          if (!empty($items)) {
            // check the first item of the field; if the bundle is among the ones for which ads are shown, loop through all items
            if (in_array($items[0]['taxonomy_term']->vocabulary_machine_name, array_filter($revive_taxonomy_term_bundles))) {
              foreach ($items as $item) {
                $tids[] = $item['tid'];
              }
            }
          }
        }
        $invocation_string .= '&tid='.implode(',', $tids);
      }
      if (module_exists('videojs_vast_vpaid')) {
        videojs_vast_vpaid_load_ads('revive', $invocation_string);
      }
    }
}

/**
 * @} End of "defgroup revive".
 */
